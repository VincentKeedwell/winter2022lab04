import java.util.Scanner;

public class Shop
{
	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
		Album[] albums = new Album[4];
		
		//User inputs the info on the album they want to input
		for (int i = 0; i < 4; i++)
		{
			System.out.println("Current Album: Album #"+(i+1));
			
			System.out.println("Enter The Name Of The Album");
			String name = reader.nextLine();
			System.out.println("Enter The Artist Of The Album");
			String artist = reader.nextLine();
			System.out.println("Enter The Price Of The Album");
			double price = Double.parseDouble(reader.nextLine());
			System.out.println("Enter The Release Year Of The Album");
			int releaseYear = Integer.parseInt(reader.nextLine());
			
			albums[i] = new Album(name, artist, price, releaseYear);
		}
		
		//Printing the info the user gave us on the last album
		System.out.println("The Name Of The Last Album You Entered is: "+albums[3].getName());
		System.out.println("The Artist Of The Last Album You Entered is: "+albums[3].getArtist());
		System.out.println("The Price Of This Album is: "+albums[3].getPrice());
		System.out.println("The Release Year Of This Album is: "+albums[3].getReleaseYear());
		
		//User re-inputs last album
		System.out.println("Oh No! Looks like you need to re-write the last album!");
		System.out.println("Enter The Name Of The Album");
		albums[3].setName(reader.nextLine());
		System.out.println("Enter The Artist Of The Album");
		albums[3].setArtist(reader.nextLine());
		System.out.println("Enter The Price Of The Album");
		albums[3].setPrice(Double.parseDouble(reader.nextLine()));
		System.out.println("Enter The Release Year Of The Album");
		albums[3].setReleaseYear(Integer.parseInt(reader.nextLine()));
		
		//Printing the new info the user gave us on the last album
		System.out.println("The New Name Of The Last Album You Entered is: "+albums[3].getName());
		System.out.println("The New Artist Of The Last Album You Entered is: "+albums[3].getArtist());
		System.out.println("The New Price Of This Album is: "+albums[3].getPrice());
		System.out.println("The New Release Year Of This Album is: "+albums[3].getReleaseYear());
		
		//Checking if there is a discount on the last album using the checkDiscount() method
		if (albums[3].checkDiscount(albums[3].getArtist()))
		{
			System.out.println("You're a DragonForce fan too?! Have a $5.00 discount!");
		}
	}
}