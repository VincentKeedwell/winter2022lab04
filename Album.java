public class Album
{
	private String name;
	private String artist;
	private double price;
	private int releaseYear;
	
	public Album(String name, String artist, double price, int releaseYear)
	{
		this.name = name;
		this.artist = artist;
		this.price = price;
		this.releaseYear = releaseYear;
	}
	
	public String getName() {
		return this.name;
	}
	public String getArtist() {
		return this.artist;
	}
	public double getPrice() {
		return this.price;
	}
	public int getReleaseYear() {
		return this.releaseYear;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	public void setArtist(String newArtist) {
		this.artist = newArtist;
	}
	public void setPrice(double newPrice) {
		this.price = newPrice;
	}
	public void setReleaseYear(int newReleaseYear) {
		this.releaseYear = newReleaseYear;
	}
	
	public boolean checkDiscount(String artist)
	{
		boolean discount = false;
		
		if (this.artist.equals("DragonForce"))
		{
			discount = true;
		}
		
		return (discount);
	}
}